## https://firebase.googleblog.com/2017/07/accessing-database-from-python-admin-sdk.html
from math import sin, cos, sqrt, atan2, radians

import requests
import firebase_admin
from firebase_admin import credentials
from firebase_admin import db
from firebase_admin import firestore


import json
import datetime 
cred = credentials.Certificate('digitalpass-389f1-firebase-adminsdk-y4u63-2954c64da0.json')
firebase_admin.initialize_app(cred, {
    'databaseURL' : 'https://digitalpass-389f1.firebaseio.com/'
})
database = firestore.client()
users_ref = database.collection('Users')
blackListedConfigArea=['WhiteField','Mumbai']
blackListedConfig=[[12.9934525,77.7152667],[12.9935638,77.7152788]]
alreadySent={}
stats = {}
def distance(lat1,lon1,lat2,lon2):

# approximate radius of earth in km
    R = 6373.0 # k 

    lat1 = radians(lat1)
    lon1 = radians(lon1)
    lat2 = radians(lat2)
    lon2 = radians(lon2)

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    distance = R * c
    return distance
def checkDistanceAndNotify(lat,lon, uid):
    if(not uid in stats.keys()):
        stats[uid]={}
    index=0
    for blackListed in blackListedConfig:
        dist = distance(lat,lon,blackListed[0],blackListed[1])
        print('Dist is ',dist)
        isRed=False
        if(dist<1):
            print('He went in danger zone , lets notify')
            if(not uid in alreadySent.keys()):
                alreadySent[uid]=1
                notifyUserViaPushNotification(uid)
            isRed=True 
        if(isRed):
            stats[uid][blackListedConfigArea[index]]=True
        index+=1

        
def notifyUserViaPushNotification(uid):
   
    docs = users_ref.stream()

    for doc in docs:
        if(str(doc.id)==str(uid)):
            print('yes')
            newDoc = doc.to_dict()
            token = json.loads(newDoc['token'])
            print(token['token'])
            if(not token['token'] in alreadySent.keys()):
                sendNotification(token['token'])
            # token = token.to_dict()
            # print(token)
            #print(str(newDoc['token']))
def sendNotification(token):
    url = "https://fcm.googleapis.com/fcm/send"
    alreadySent[token]=1
    payload = "{\n    \"to\": \""+token+"\",\n    \"notification\": {\n      \"title\": \"Digital Pass\",\n      \"body\": \"You just went to a danger zone\",\n      \"mutable_content\": true,\n      \"sound\": \"Tri-tone\"\n      },\n\n   \"data\": {\n    \"url\": \"\",\n    \"dl\": \"\"\n      }\n}"
    headers = {
        'Authorization': "key=AAAATZ4M4G8:APA91bHwzrsmVBspfgT8RRiA98HLPVrgxCOFQATWGE86dEqRoO37oW1IpFW2v3K096up5yyYyR-Sl5nwwpaYSY9IOW9yOEa9QcdQcMMewUP0_CoO8vBsp5aQNzMbqh8Q8E0JJxyx1LAn",
        'Content-Type': "application/json",
        'Cache-Control': "no-cache"
        }
    try:
        response = requests.request("POST", url, data=payload, headers=headers)
    except:
        print("unable to send")
    print(response.text)
    return True


users = db.reference('geolocation').get()
#print(users)

for user in users.keys():
    print(user)
    for record in users[user]:
        try:
            lat = users[user][record]['location']['location']['coords']['latitude']
            lon = users[user][record]['location']['location']['coords']['longitude']
            checkDistanceAndNotify(lat,lon, user)
        except:
            print('Bad data')        
        print(lat,lon)
    print(stats)
    for key in stats.keys():
        doc_ref = users_ref.document(key)
        doc_ref.update({u'isRed' : stats[key]})

        print(key)