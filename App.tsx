import * as React from 'react';
import AuthLoadingScreen from './src/screens/AuthLoading';

import { Provider } from 'react-redux';
import { createStore } from 'redux';
import rootReducer from './src/reducers';
import { Root } from 'native-base';
import { persistStore, persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';

import { PersistGate } from 'redux-persist/integration/react';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage
}
const persistedReducer = persistReducer(persistConfig, rootReducer);

let store = createStore(persistedReducer);
let persistor = persistStore(store);

interface Props { }
interface State { }
class App extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    console.disableYellowBox = true;
  }

  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <Root>
            <AuthLoadingScreen />
          </Root>
        </PersistGate>

      </Provider>
    );
  }
}

export default App; 