import * as React from 'react';
import { View, Text } from 'react-native';
import auth from '@react-native-firebase/auth';
import { connect } from 'react-redux'
import { userLogoutSuccess, userLogoutSuccessPayload } from '../../actions/auth';
import App from '../../../App';

export interface Props {
  navigation?: any;
  userLogoutSuccess: any;
}

interface State {
  counter: number;
}
class LogoutScreen extends React.Component<Props, State> {
  props: Props;
  state: State;
  static timerId: number;
  constructor(props: Props) {
    super(props);
    this.props = props;
    
    auth()
      .signOut()
      .then(() => {
      })
      .catch((e)=>{
        console.log('Facebook auth error',e)
      });

      let payLoad: userLogoutSuccessPayload = {
        OnSuccess: () => {
           // App.persistor.purge();
        },
        OnFailure: () => { },
        data: ""
      }
      this.props.userLogoutSuccess(payLoad);

    this.state = { counter: 5 }

  }


  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'flex-start', padding: 20 }}>
        <View style={{ marginTop: 40 }}>

          <Text style={{ fontSize: 36, fontWeight: 'bold' }}>
            {this.state.counter > 0 ? `Logging you out in  ${this.state.counter} seconds ` : 'Something Went Wrong !'}

          </Text>
        </View>
      </View>
    );
  }
}
const mapStateToProps = (state: any) => {
  const { auth } = state;
  return {
    auth: auth,
    //  userProfile: userProfile.data || []
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    userLogoutSuccess: (payload: userLogoutSuccessPayload) => dispatch(userLogoutSuccess(payload))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps)(LogoutScreen)
