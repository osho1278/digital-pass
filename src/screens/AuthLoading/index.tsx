import * as React from 'react';
import { connect } from 'react-redux'

import { View, Text, Dimensions } from 'react-native';
import { Profile } from '../../models/profile'
import { LoginScreen } from '../Auth/login';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import HomeScreen from '../Home/home';
import { OnPassSelected } from '../Home/onPassSelected';
import { HelpScreen } from '../Help';
import { PassesList } from '../../components/PassList'
import RequestNewPass from '../RequestNewPass';
import LogoutScreen from '../Logout';
import AdminApproveReject from '../AdminApproveReject';
import {
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem
} from '@react-navigation/drawer';
import Icon from 'react-native-vector-icons/Ionicons';
import { DrawerButton } from '../../components/Drawer';
import { ScanScreen } from '../../components/QRScanner';
import Logout from '../Logout';
const { width, height } = Dimensions.get('window');
import auth from '@react-native-firebase/auth';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
interface Props {
  profile?: Profile;
  navigation?: any;
}

interface State {
  profile?: Profile;
  isAuthenticated: boolean;
}
class AuthLoadingScreen extends React.Component<Props, State> {

  props: Props;

  constructor(props: Props) {
    super(props);
    this.props = props;
    this.state = { isAuthenticated: false };
    // if(!this.props.profile){
    //   this.props.navigation.navigate('Login');
    // }
    auth().onAuthStateChanged(this.authStateChangeHandler)

  }
  authStateChangeHandler = (user: any) => {
    if (!user) {
      this.setState({ isAuthenticated: false });
    } else {
      this.setState({ isAuthenticated: true });
    }
  }
  componentWillReceiveProps(props: Props) {
    this.props = props;
  }

  authStackNavigator = () => {
    return (
      <Stack.Navigator>
        <Stack.Screen name="Login" component={LoginScreen} options={{ header: undefined }} />
      </Stack.Navigator>);
  }
  // @ts-ignore
  homeStackNavigator = ({ navigation }) => {
    const headerOptions: any = {
      // headerTitle: () => <View style={{ alignItems: 'center' }} ><Text>{`Osho`}</Text></View>,
      headerLeft: () => {
        return (<DrawerButton navigation={navigation} />)
      }
    }
    return (
    
        <Stack.Navigator >
          <Stack.Screen name="Home" component={HomeScreen} options={{ ...headerOptions }} />
          <Stack.Screen name="Request New Pass" component={RequestNewPass} />
          <Stack.Screen name="Approve/Reject" component={AdminApproveReject} />
          <Stack.Screen name="Pass List" component={PassesList} />
          <Stack.Screen name="My Pass" component={OnPassSelected} options={{ ...headerOptions }} />
          <Stack.Screen name="Scan" component={ScanScreen} options={{ ...headerOptions }} />
          <Stack.Screen name="Logout" component={LogoutScreen} options={{ ...headerOptions }} />
        </Stack.Navigator>
     
    )
  }



  CustomDrawerContent = (props: any) => {
    return (

      <DrawerContentScrollView {...props}>
        <View style={{ padding: 30, justifyContent: 'center', flex: 1, flexDirection: "row", backgroundColor: '#292b2c' }}>
          <Icon name="md-qr-scanner" size={30} color="#4F8EF7" style={{ marginTop: 8 }} />
          <Text style={{ fontSize: 32, color: '#4F8EF7' }}>{`  Digital Pass`}</Text>
        </View>
        <View
          style={{
            borderBottomColor: 'cyan',
            borderBottomWidth: 1,
          }}
        />
        <View style={{ padding: 10 }}>
          <DrawerItemList {...props} />
        </View>

        <View
          style={{
            borderBottomColor: 'black',
            borderBottomWidth: 0.5,
          }}
        />

        <DrawerItem label="Logout"
          labelStyle={{ color: 'black', backgroundColor: '#d9534f', borderRadius: 5, padding: 10, paddingRight: 1000, textAlign: 'auto', width: '100%', fontWeight: 'bold' }}
          onPress={() => props.navigation.navigate('Logout')} />
        <View style={{ flex: 1, padding: 20 }}>
          <View style={{ flex: 1, flexDirection: 'row', marginTop: height / 2, justifyContent: 'center', alignItems: 'center' }}>
            <Icon name="ios-information-circle" size={30} color="#4F8EF7" style={{ marginTop: 8 }} />

            <Text style={{ fontSize: 12, color: 'grey' }}>{`   All rights reserved @osho1278`}</Text>
          </View>
        </View>

      </DrawerContentScrollView>

    );
  }





  homeDrawerNavigator = () => {
    const headerOptions: any = {
      // headerTitle: () => <View style={{ alignItems: 'center' }} ><Text>{`Osho`}</Text></View>,
      // headerLeft: () => {
      //   return (<DrawerButton />)
      //  }
    }

    return (
        <Drawer.Navigator
          initialRouteName='Home'
          drawerContent={props => this.CustomDrawerContent(props)}
        >

          <Drawer.Screen name="Home" component={(props) => this.homeStackNavigator(props)} options={{ ...headerOptions }} />
          <Drawer.Screen name="Help" component={HelpScreen} options={{ ...headerOptions }} />
        </Drawer.Navigator>
   
    );
  }

  render() {
    return (
      <NavigationContainer >
        {
          this.state.isAuthenticated && this.props.profile ?
            this.homeDrawerNavigator()
            :
            this.authStackNavigator()

        }

      </NavigationContainer>


    );
  }
}
const mapStateToProps = (state: any) => {
  const { auth } = state;
  let _user: any = undefined;
  if (auth) {
    _user = auth && auth._user || undefined;
  }
  console.log('State at auth loading is ', _user);
  return {
    profile: _user || undefined
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {

  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps)(AuthLoadingScreen)
