import * as React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import  FacebookSignIn  from '../../components/Auth/facebook';
import { Thumbnail } from 'native-base';
import LinearGradient from "react-native-linear-gradient";
import CustomLoginView from '../../components/Auth/CustomLogin/emailPassword';

interface  Props{
  navigation?:any;
  onSignInSuccess?:Function;
}
export class LoginScreen extends React.Component<Props> {
  constructor(props:Props){
    super(props);
   
  }
  componentDidMount(){
    this.props.navigation.setOptions({
        header: ()=>null
      })
  }

  render() {
    
    return (
      <View style={{ 
        flex: 1,
        padding:20,
        alignItems: 'center', 
        backgroundColor: "#193F78"
       }}>
             <View style={{justifyContent:'flex-start'}}>
             <Text style={{
               fontSize:52, 
              paddingTop:0, 
              color:'white'}}>
                {`DIGITAL PASS`}
                </Text>
            
             </View>

           <View style={{justifyContent:'center', marginTop:50 }}>
          
         <Text style={{ fontSize: 30, color: "#fff" }}>{'Welcome back!'}</Text>
         <Text style={{ fontSize: 16, color: "#fff" }}>{'You can apply for a digital pass with your nearest police station in 20 seconds or faster !'}</Text>
         {/* <Thumbnail source={require('./background.png')} resizeMode="contain" style={{ padding:10 }} /> */}
              </View>
          <CustomLoginView/>    
          <FacebookSignIn/>
       
          <Text style={{ fontSize: 14, color: "#fff" }}>{'We will only collect your publicly available information !'}</Text>

       
      </View>
    );
  }
}