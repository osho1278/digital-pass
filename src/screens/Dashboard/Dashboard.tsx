import * as React from 'react';
import { View, Text } from 'react-native';

interface Props {
  navigation: any;
}

export class DashboardScreen extends React.Component<Props> {

  constructor(props: Props) {
    super(props);
    console.log('DashboardScreen props', props.navigation);
  }

  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>{`This is Dashbaord`}</Text>
      </View>
    );
  }
}
