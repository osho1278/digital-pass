import React from 'react';
import { View, RefreshControl,Alert } from 'react-native';
import { List, ListItem, Left, Body, Right, Toast, Text, Thumbnail } from 'native-base';
import firestore from '@react-native-firebase/firestore';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import { DigitalPassModal } from '../../components/Dialog/Modal';
import {connect} from 'react-redux';
import { Location } from '../../models/location';
import { distance } from '../../utils/geolocation';
interface Props {
  profile?: any;
  navigation?: any;
  myLocation:Location;
}
interface State {
  users?: any[];
  refreshing: boolean,
  showDialog:boolean,
  uid:string;
  index?:number;
  reason?:string;
  item:any;
}
class AdminApproveReject extends React.Component<Props, State>{

  constructor(props: Props) {
    super(props);
    console.log("AdminApproveReject",props);
    this.state = { users: [], refreshing: false ,showDialog:false,uid:'', item:undefined};
  }
  onRefresh = () => {
    // this.setState({ refreshing : !this.state.refreshing});
    this.getPassInformation();
  }
  getPassInformation = async () => {
    this.setState({ refreshing: true });
    try {
      // const { uid } = this.props.profile;
    await firestore()
        .collection('Users')
        .get()
        .then((user) => {
          let users = user.docs.map((item: any) => item._data);
          this.setState({ users });

          this.setState({ refreshing: false });
        })


    } catch (e) {
      console.log('Cannot get user profile , proceed to login', e);
      // this.props.navigation.navigate('Login');
    }
  }

  onPassClickHandler=(item:any)=>{
    this.setState({showDialog:true, item, uid:item.uid, index:item.index});
    // this.setState({showDialog:true});
  }

  handleApprove=async (reason:string)=>{
    this.onRefresh();
    this.updatePass(reason,true);
    this.setState({showDialog:false});
  }
  handleReject=(reason:string)=>{
    this.onRefresh();
    this.updatePass(reason,false);
  }
  updatePass=async (reason:string , isApproved:boolean)=>{
    let user =  await firestore()
    .collection('Users')
    .doc(this.state.uid)
   .get()

   // const  passIndex= (this.state.index || 0) -1;
   try{
    // @ts-ignore
    let { passes } = user._data;
    passes[this.state.index]["isApproved"]=isApproved;
    passes[this.state.index]["adminReason"]=reason || '';
    await firestore()
    .collection('Users')
    .doc(this.state.uid)
    .update({
      // @ts-ignore
      passes:passes
    })
  }
    catch(e){
     console.log(e);
    }
    this.setState({showDialog:false});
  }
  componentDidMount() {

    this.getPassInformation();
  }

  getMyDistanceFromItem=(item:Location)=>{
    const myLat = this.props.myLocation.coords && this.props.myLocation.coords.latitude;
    const myLon = this.props.myLocation.coords && this.props.myLocation.coords.longitude;

    const opLat= item && item.coords && item.coords.latitude ;
    const opLon= item && item.coords && item.coords.longitude ;
    
    if(myLat || myLon || opLon || opLat){
      console.log(myLat,myLon,opLat,opLon);
      let distanceInkms = Math.floor(distance(myLat, myLon , opLat, opLon, "K"))
      return isNaN(distanceInkms) ? 'unknown' : distanceInkms.toString();
    }
    return 'unknown';
  }


  render() {
    return (
      <ScrollView refreshControl={
        <RefreshControl refreshing={this.state.refreshing} onRefresh={this.onRefresh} />
      }>
      <View style={{ flex: 1, padding: 10 }}>
        {this.state.users && this.state.users.length > 0 ?
         
            <List>
              {this.state.users.map((item: any, index: number) => {
                 const {isRed} = item;  
                return (
                  <TouchableOpacity key={`${index}`} onPress={() => {       
                    if(isRed){
                      // console.log(isRed);
                      let stri:string='';
                      let ct:number=0;
                      for (let key of Object.keys(isRed)){
                        ct+=1
                        stri+=key+' '

                      }
                      Alert.alert(`User has been to red zone ${Object.keys(isRed).length} times \n Description : {${stri}}`);
                    }
                    this.props.navigation.push('Pass List',
                    {
                      profile:item , 
                    onPassClickHandler: this.onPassClickHandler
                  })

                  }}>
                    <ListItem avatar>
                      <Left>
                        <Thumbnail source={{ uri: item.photoURL }} />
                      </Left>
                      <Body>
                        <Text>{item.displayName}</Text>
                        <Text note>{item.email}</Text>
                        <Text note style={{color:'green'}}>{`${this.getMyDistanceFromItem(item.location)} kms away`}</Text>
                      </Body>
                      <Right>
                                                
                        { isRed && Object.keys(isRed).length > 0 && (<Text style={{color:'red'}}>{`RED`}</Text>)}    
                         
                      </Right>
                    </ListItem>

                  </TouchableOpacity>
                )
              })}
            </List>
         
          :
          <View style={{ flex: 1, justifyContent: 'center', padding: 20, }}>
            <Text style={{ fontSize: 22, fontWeight: 'bold' }}>
              {`Loading `}
            </Text>
          </View>
        }

        {this.state.showDialog && <DigitalPassModal 
        mode={'edit'}
         title={'Approve/Reject'}
         item={this.state.item}
         description={'Confirmation needed'}
         showDialog={this.state.showDialog}
         handleApprove={this.handleApprove}
         handleReject={this.handleReject}
        />
  }
      </View>
      </ScrollView>
    );
  };
}


const mapStateToProps = (state: any) => {
  console.log('State',state);''
  const { location } = state;  
  return {
    myLocation: location
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    // userUpdateLocationSuccess: (payload: userUpdateLocationPayload) => dispatch(userUpdateLocationSuccess(payload))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps)(AdminApproveReject)
