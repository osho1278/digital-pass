import React from 'react';
import { View, Text, Button, TextInput, StyleSheet } from 'react-native';
import QRCode from 'react-native-qrcode-svg';
import moment from 'moment';
import {Pass} from '../../models/pass';

interface Props {
  pass:Pass;
  navigation?: any;
  route?:any;

}
interface State {
  pass:Pass;
}
export class OnPassSelected extends React.Component<Props,State>{


  constructor(props:Props){
    super(props);
    console.log(props);
    const {route} = props;
    const {params} = route;
    this.state={pass:params};
  }
  componentDidMount(){
  }

  render() {
    const {reason, fromDate , toDate , isApproved} = this.state.pass;
    return (
        <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }} >
          <View style={{ flex: 1, paddingTop: 10, alignItems: 'center' }} >
            <Text style={{ fontSize: 22, fontWeight: 'bold' }}>{`${reason}`}</Text>
            <Text style={{ fontSize: 18 }}>{`Valid upto : ${moment.unix(toDate).format("dddd, MMMM Do YYYY, h:mm:ss a")}`}</Text>
            <Text style={{ fontSize: 14 }}>{`( Expires ${moment.unix(toDate).fromNow()})`}</Text>
          </View>
          <View style={{ flex: 1, backgroundColor: 'red', justifyContent: 'center' }}>
            <QRCode
              value={JSON.stringify(this.state.pass)}
              size={300}
            />
          </View>
          <View style={{ flex: 1, padding: 20, justifyContent: 'flex-end' }} >
            <Text style={{ color: 'grey' }}>{`I hereby confirm that i would neither transfer this QR code nor use this for any work other than mentioned above.`}</Text>
          </View>
        </View>
    );
  };
}
