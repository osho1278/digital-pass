import React from 'react';
import { View, Button, RefreshControl } from 'react-native';
import { List, ListItem, Left, Body, Right, Toast, Text } from 'native-base';
import { connect } from 'react-redux'
import firestore from '@react-native-firebase/firestore';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import { Pass } from '../../models/pass';
import Icon from 'react-native-vector-icons/Ionicons';
import { PassesList } from '../../components/PassList';
import Geolocation from '@react-native-community/geolocation';
import {check, RESULTS,request, PERMISSIONS} from 'react-native-permissions';
import { updateUser } from '../../utils/user';
import { userUpdateLocationPayload, userUpdateLocationSuccess } from '../../actions/location';
import RemotePushController from '../../services/pushNotification';
import { updateGeoLocation } from '../../utils/geolocation';

interface Props {
  profile?: any;
  navigation?: any;
  userUpdateLocationSuccess:Function;
}
interface State {
  profile?: any;
  reason?: string;
  passes?: any[];
  validity?: string; // '2020-04-20T00:00:00Z',
  refreshing: boolean
  shouldUpdate?:boolean;
}
class HomeScreen extends React.Component<Props, State>{

  constructor(props: Props) {
    super(props);
    this.state = { profile: props.profile, passes: [], refreshing: false,shouldUpdate:false };
    props.navigation.setOptions({
      headerRight: () => {
        return (<Icon
          name="md-qr-scanner" size={30}
          color="#4F8EF7"
          style={{ marginTop: 8, paddingRight: 20 }}
          onPress={()=>props.navigation.navigate('Scan')}
          />
        )
      }
    })
   //  BgTracking.uid = props.profile.uid || '';
    this.updateLocation();

  }

  updateLocation=async ()=>{
   // Geolocation.requestAuthorization()
   check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
  .then((result) => {
    switch (result) {
      case RESULTS.UNAVAILABLE:
        request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION).then((result) => {
          this.updateLocation();
        });
        break;
      case RESULTS.DENIED:
        request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION).then((result) => {
          this.updateLocation();
        });
        break;
      case RESULTS.GRANTED:
        console.log("Permission was already granted");
        Geolocation.getCurrentPosition((info) => {
          console.log(info)
          this.updateState(info)
          updateUser(this.props.profile.uid , {location:info})  ;
        },
        error => console.log(error), 
        { enableHighAccuracy:false,timeout: 2000, maximumAge: 1000000 });
        Geolocation.watchPosition((info) => {
          console.log('Watching',info)
          // this.updateState(info)
        //  updateUser(this.props.profile.uid , {location:info})  ;
          updateGeoLocation(this.props.profile.uid , {location:info});
        },
        error => console.log(error), 
        { enableHighAccuracy:false,timeout: 2000  , maximumAge: 100000 });

        break;
      case RESULTS.BLOCKED:
        console.log('The permission is denied and not requestable anymore');
        break;
    }
  })
  .catch((error) => {
    console.log(error);
  });
   

  }



  updateState=(info:any)=>{
    console.log(info);
    let payload:userUpdateLocationPayload={
      OnSuccess:()=>{console.log('Location updated to state');},
      OnFailure:()=>{console.log('Location cannot be updated to state');},
      data:info

    }
    this.props.userUpdateLocationSuccess(payload);

  }
  onRefresh = () => {
    this.setState({ shouldUpdate:true});

    this.getPassInformation();
    this.setState({ shouldUpdate:false});
  }

  onRequestNewPassSuccess=()=>{
    Toast.show({
      text: "You have successfully applied for the pass. Wait for admin to approve.",
      buttonText: "Okay",
      duration: 10000
    })
    this.onRefresh();
  }

  getPassInformation = async () => {
    this.setState({ refreshing: true });
    try {
      const { uid } = this.props.profile;
      const user = await firestore()
        .collection('Users')
        .doc(uid)
        .get();
      // @ts-ignore
      let { passes } = user._data;
      this.setState({ passes: passes });
      this.setState({ refreshing: false,shouldUpdate:false });

    } catch (e) {
      this.setState({ refreshing: false,shouldUpdate:false });
      this.props.navigation.navigate('Login');
    }
  }

  componentDidMount() {
    if(!this.props.profile){
      this.props.navigation.goBack();
    }
    this.getPassInformation();
  }

  componentWillReceiveProps(props:Props){
    if(!props.profile){
      this.props.navigation.goBack();
    }
    this.getPassInformation();
  }

  onPassClickHandler = (item: any) => {
    item.isApproved ?
      this.props.navigation.navigate('My Pass', { ...item })

      : Toast.show({
        text: "This pass has not been approved yet!",
        buttonText: "Okay",
        duration: 3000
      })
  }


  render() {
    const { profile, navigation } = this.props;
    console.log(profile);
    return (
      <View style={{ flex: 1, padding: 10 }}>
        <RemotePushController uid={this.props.profile.uid}/>
        {/* <BgTracking/> */}
        <Text style={{ fontSize: 22, fontWeight: 'bold' }}>{`My Passes`}</Text>
        <PassesList shouldUpdate={this.state.shouldUpdate}profile={profile} navigation={navigation} onPassClickHandler={this.onPassClickHandler} />
        <View style={{ marginBottom: 20, flexDirection: 'row', justifyContent: 'space-evenly' }}>
          <Button
            title="Request a new Pass"
            color="orange"
            onPress={() => this.props.navigation.navigate('Request New Pass',{onRequestNewPassSuccess:this.onRequestNewPassSuccess})}
          />
          { profile && profile.role && profile.role === 'ADMIN' &&
          <Button
            title="Approve/Reject"
            onPress={() => this.props.navigation.navigate('Approve/Reject')}
          />
  }
        </View>
      </View>
    );
  };
}

const mapStateToProps = (state: any) => {
  const { auth } = state;
  let _user: any = undefined;
  if (auth) {
    _user = auth && auth._user;
  }
  return {
    profile: _user
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    userUpdateLocationSuccess: (payload: userUpdateLocationPayload) => dispatch(userUpdateLocationSuccess(payload))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps)(HomeScreen)
