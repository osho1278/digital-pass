import * as React from 'react';
import { View, Text } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

export class HelpScreen extends React.Component {
  render() {
    return (
      <View style={{flex:1}}>
      <View style={{ flex: 1, justifyContent:'flex-start', padding:20 }}>
        <View style={{ marginTop:40 }}>

          <Text style={{ fontSize: 36, fontWeight: 'bold' }}>
            {`How can I help you today  ? `}

          </Text>
        </View>
        <View style={{flexDirection: 'row',marginTop:40 }}>
          <Icon name="ios-mail" size={30} color="#4F8EF7" />
          <Text style={{ fontSize: 22, fontWeight: 'bold' }}>
            {`    osho1278@gmail.com `}
          </Text>
        </View>
        <View style={{ flexDirection: 'row',marginTop:40 }}>
          <Icon name="logo-twitter" size={30} color="#4F8EF7" />
          <Text style={{ fontSize: 22, fontWeight: 'bold' }}>
            {`    @osho1278`}
          </Text>
        </View>
      </View>
 <View style={{ justifyContent:'flex-end',padding:20 }}>

 <Text style={{ fontSize: 18 , color:'grey' }}>
   {`Created with all love in the world by @osho1278`}

 </Text>
 </View>
 </View>
    );
  }
}
