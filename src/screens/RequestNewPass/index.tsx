import React from 'react';
import { connect } from 'react-redux'

import { View, Button, Dimensions, Text, TextInput, StyleSheet, Modal, Image, RefreshControl } from 'react-native';
import moment from 'moment';
import { DigitalPassDatePicker } from '../../components/DatePicker';
import { DigitalPassTextInput } from '../../components/Input';
import firestore from '@react-native-firebase/firestore';
import { DigitalPassCamera } from '../../components/Camera';
import { ScrollView } from 'react-native-gesture-handler';
import storage from '@react-native-firebase/storage';
import { Picker } from 'native-base';

const { width, height } = Dimensions.get('window');

interface Props {
  navigation?: any;
  profile?: any;
}
interface State {
  profile?: any;
  reason: string;
  fromDate: number;
  toDate: number;
  uploadID: boolean;
  uploadSelfie: boolean;
  idImage: string;
  selfieImage: string;
  refreshing: boolean;
}
class RequestNewPass extends React.Component<Props, State>{
  constructor(props: Props) {
    super(props);
    this.state = {
      profile: props.profile,
      reason: '',
      fromDate:  new Date().getTime() / 1000,
      toDate: (new Date().getTime() + (3600 * 24*1000))/1000,
      uploadID: false,
      uploadSelfie: false,
      selfieImage: '',
      idImage: '',
      refreshing: false
    };
  }
  makeid = (length: number) => {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }
  onReasonEntered = (reason: string) => {
    this.setState({ reason })
  }
  onFromDateSelected = (fromDate: number) => {
   
    const epoch = new Date(fromDate).getTime() / 1000;

    this.setState({ fromDate: epoch })
  }
  onToDateSelected = (toDate: number) => {
   
    const epoch = new Date(toDate).getTime() / 1000;
    this.setState({ toDate: epoch })
  }

  onUploadIdPressed = () => {
    this.setState({ uploadSelfie: false, uploadID: !this.state.uploadID })
  }
  onUploadSelfiePressed = () => {
    this.setState({ uploadID: false, uploadSelfie: !this.state.uploadSelfie })
  }

  uploadImageToFirebaseStorage = async (uid: string, imagePath: string, typeOfImage: string) => new Promise((resolve, reject) => {
    let randId = this.makeid(10)
    const reference = storage().ref('/images/' + uid + '/' + typeOfImage + '_' + randId);
    try {
      const task = reference.putFile(imagePath);

      task.on('state_changed', taskSnapshot => {
        // @ts-ignore
        console.log(`${taskSnapshot.bytesTransferred} transferred out of ${task.totalBytes}`);
      });

      task.then(async () => {
        // @ts-ignore
        let imgPath = await reference.getDownloadURL()
        resolve(imgPath);

      });
    } catch (e) {
      reject(e);
    }

  });

  onSubmitButtonPressed = async () => {
    this.setState({ refreshing: true })
    const payload: any = {
      reason: this.state.reason,
      fromDate: this.state.fromDate,
      toDate: this.state.toDate,
      isApproved: false
    }

    // get old passes if already there 

    const { uid } = this.props.profile;

    let idPath;
    let selfiePath;

    if (this.state.idImage.startsWith('file')) {
      idPath = await this.uploadImageToFirebaseStorage(uid, this.state.idImage, 'id');
      payload.image_id = idPath;
    }
    if (this.state.selfieImage.startsWith('file')) {
      selfiePath = await this.uploadImageToFirebaseStorage(uid, this.state.selfieImage, 'selfie');
      payload.image_selfie = selfiePath;
    }

    const user = await firestore()
      .collection('Users')
      .doc(uid)
      .get();
    // @ts-ignore
    let { passes } = user._data;
    
    if (passes) {
      passes.push(payload);
      firestore()
        .collection('Users')
        .doc(uid)
        .update({
          passes
        })
        .then(() => {
          this.setState({ refreshing: false })
          try {
            // @ts-ignore
            this.props.route.params.onRequestNewPassSuccess();
          } catch (e) {
            console.log('Unable to refresh',e);
          }
          this.props.navigation.goBack();
        });
    } else {
      firestore()
        .collection('Users')
        .doc(uid)
        .update({
          passes: [payload]
        })
        .then(() => {
          this.setState({ refreshing: false })
          try {
            // @ts-ignore
            this.props.route.params.onRequestNewPassSuccess();
          } catch (e) {
            console.log('Unable to refresh',e);
          }
          this.props.navigation.goBack();
        });
    }
  }


  render() {
    return (
      <ScrollView refreshControl={
        <RefreshControl refreshing={this.state.refreshing} />
      }
      >
        <View style={{ padding: 20, paddingTop: 10, flex: 1, justifyContent: 'flex-start', alignItems: 'flex-start', flexDirection: 'column' }}>
          <View style={{ ...styles.viewStyle, flexDirection: 'row', justifyContent: 'center' }}>

            <Text style={{ ...styles.label, color: 'black' }}>{`Reason : `}</Text>

            <Picker
              note
              mode="dropdown"
              style={{ flex: 1 }}
              selectedValue={this.state.reason}
              onValueChange={(text: any) => this.setState({ reason: text })}
            >
              <Picker.Item label="Select a reason" value="Select a reason" />
              <Picker.Item label="Medical or Health" value="Medical or Health" />
              <Picker.Item label="Death in Family" value="Death in Family" />
              <Picker.Item label="Business or Trade" value="Business orTrade" />
              <Picker.Item label="Animal Care activity" value="Animal Care activity" />
              <Picker.Item label="Volunteer Activity" value="Volunteer Activity" />
            </Picker>
          </View>
          <View>
            <DigitalPassTextInput
              label={'Reason'}
              placeholder={'My reason is not mentioned above'}
              onInputChange={this.onReasonEntered}
            />
          </View>
          <View style={styles.viewStyle}>
            <DigitalPassDatePicker label={'From Date'} onDateSelected={this.onFromDateSelected} default={Date.now()} />
            <Text style={styles.label}>{`${moment.unix(this.state.fromDate).format("dddd, MMMM Do YYYY, h:mm:ss a")}`}</Text>

          </View>
          <View style={styles.viewStyle}>
            <DigitalPassDatePicker label={'To Date'} onDateSelected={this.onToDateSelected} default={Date.now()} />
            <Text style={styles.label}>{`${moment.unix(this.state.toDate).format("dddd, MMMM Do YYYY, h:mm:ss a")}`}</Text>
          </View>
          <View style={{ flex: 1, flexDirection: 'column', marginRight: 100 }}>
            <Button
              title="Upload Personal ID"
              onPress={() => this.onUploadIdPressed()}
            />
            <View style={{ height: 250, width: width - 50, padding: 5 }}>
              {
                this.state.idImage ?
                  <Image
                    style={{ flex: 1, borderWidth: 2 }}
                    source={{
                      uri: this.state.idImage
                    }} />
                  :
                  <Text>{'Uploaded Id will appear here'}</Text>
              }
            </View>
          </View>
          <View style={{ flex: 1, flexDirection: 'column', padding: 5 }}>
            <Button
              title="Upload Selfie"
              onPress={() => this.onUploadSelfiePressed()}
            />
            <View style={{ height: 250, width: width - 50, padding: 5 }}>
              {this.state.selfieImage ?
                <Image
                  style={{ flex: 1, borderWidth: 2 }}
                  source={{
                    uri: this.state.selfieImage,
                  }} />
                :
                <Text>{'Uploaded Selfie will appear here'}</Text>
              }
            </View>
          </View>

          <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.uploadID || this.state.uploadSelfie}
            onRequestClose={() => {
              // Alert.alert("Modal has been closed.");
            }}
          >
            <View style={{ flex: 1, ...styles.modalView }}>
              <View style={{ flex: 1 }}>
                <DigitalPassCamera
                  // @ts-ignore
                  cameraType={this.state.uploadID ? 'back' : 'front'}
                  getimageUri={(uri: string) => {
                    if (this.state.uploadID) {
                      // it was an id
                      this.setState({ idImage: uri });
                      this.onUploadIdPressed()
                    } else {
                      // it was a selfie
                      this.setState({ selfieImage: uri });
                      this.onUploadSelfiePressed()
                    }
                  }} />
              </View>
            </View>
          </Modal>
        </View>

        <Button
          title="Submit"
          color='green'
          onPress={() => this.onSubmitButtonPressed()}
        />
      </ScrollView>
    )
  };
}
const styles = StyleSheet.create({
  label: {
    fontSize: 18,
    fontStyle: 'normal',
    fontWeight: '300',
    padding: 10,
    alignItems: 'flex-start',
    color: 'grey'
  },

  viewStyle: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
    alignContent: 'space-between',
    padding: 10
  },
  modalView: {
  },
});

const mapStateToProps = (state: any) => {
  const { auth } = state;
  let _user: any = undefined;
  if (auth) {
    _user = auth && auth._user;
  }
  return {
    profile: _user
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {

  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps)(RequestNewPass)
