import React, { Component } from 'react';
import { Input, Label } from 'native-base';
import {View} from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
interface Props{
  label?:string;
  onInputChange:Function;
  placeholder?:string;
  numberOfLines?:number
}

export const DigitalPassTextInput = (props:Props)=> {
 
    return (
     
           <View>
              <TextInput
              numberOfLines={props.numberOfLines || 3} 
              placeholder={props.placeholder || ''}
              multiline={true}
              onChangeText={(text)=>props.onInputChange(text)}
              style={{borderRadius:5,borderWidth:0.5 , width:'100%' , padding:10}}
              />
              </View>
    );
}