import React, { Component } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import Dialog from "react-native-dialog";

interface Props {
    title?: string;
    description?: string;
    showDialog: boolean;
    handleApprove: Function;
    handleReject: Function;
}

export const DigitalPassDialog =(props:Props)=> {
        let reason=''
        return (
            <View>
                <Dialog.Container visible={props.showDialog}>
                    <Dialog.Title>{`${props.title}`}</Dialog.Title>
                    <Dialog.Description>
                        {`${props.description}`}
                    </Dialog.Description>
                    <Dialog.Input
                    style={{borderRadius:5, borderWidth:1}}
                    placeholder={`Reason`}
                    onChangeText={(text)=>{reason = text}}
                    />
                    <Dialog.Button label="Approve" onPress={() => props.handleApprove(reason)} />
                    <Dialog.Button label="Reject" onPress={() => props.handleReject(reason)} />
                </Dialog.Container>
            </View>
        );
}