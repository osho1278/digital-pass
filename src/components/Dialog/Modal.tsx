import React, { Component, useState } from "react";
import {
    Alert,
    Modal,
    StyleSheet,
    Text,
    TouchableHighlight,
    View,
    Button,
    Image,
    Dimensions,
    BackHandler
} from "react-native";
import { DigitalPassTextInput } from "../Input";
import moment from 'moment';
const { width, height } = Dimensions.get('window');

interface Props {
    mode: string;
    title?: string;
    description?: string;
    selfieImage?: string
    idImage?: string
    showDialog: boolean;
    item?: any;
    handleApprove: Function;
    handleReject: Function;
}
export const DigitalPassModal = (props: Props) => {
    let shouldShowModal = props.showDialog;
    let adminReason = '';
    const onReasonEntered = (text: string) => {
        adminReason = text;
    }

    const { item } = props;
    const { reason, image_id, image_selfie } = item || undefined;
    return (
        item ?
            <View style={{ flex: 1, padding: 20 }}>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={shouldShowModal}
                    onRequestClose={() => {
                        // Alert.alert("Modal has been closed.");
                    }}
                >
                    <View style={{ flex: 1, padding: 20 }} >
                        <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>
                            <Text style={{ fontSize: 28, fontWeight: 'bold', paddingBottom: 10 }}>{`${props.title}`}</Text>
                        
                        </View>
                        {/* Horizontal line */}
                        <View
                            style={{
                                borderBottomColor: 'black',
                                borderBottomWidth: 1,
                            }}
                        />
                        <View style={{flexDirection: 'column', alignItems:'center', padding:10 }}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold' }}>{`${reason}`}</Text>
                            <Text style={{ fontSize: 14 }}>{`Valid upto : ${moment.unix(item.toDate).format("dddd, MMMM Do YYYY, h:mm:ss a")}`}</Text>
                            <Text style={{ fontSize: 14 }}>{`( Expires ${moment.unix(item.toDate).fromNow()})`}</Text>

                        </View>
                        <View style={{ height: image_id ? 200 : 20, width: width - 50, padding: image_id ? 5 : 20 }}>
                        <Text>{'Personal ID'}</Text>
                            {image_id ?
                            
                                <Image
                                    style={{ flex: 1, borderWidth: 2 }}
                                    source={{
                                        uri: image_id
                                    }} />
                                :
                                <Text>{'Personal id was not uploaded'}</Text>
                            }
                        </View>
                        <View style={{ height: image_selfie ? 200 : 20, width: width - 50, padding: image_selfie ? 5 : 20 }}>
                        <Text>{'Self attested Picture'}</Text>
                            {image_selfie ?
                                <Image
                                    style={{ flex: 1, borderWidth: 2 }}
                                    source={{
                                        uri: image_selfie,
                                    }} />
                                :
                                <Text>{'Personal picture was not uploaded'}</Text>
                            }
                        </View>

                        {/* Horizontal line */}
                        <View
                            style={{
                                borderBottomColor: 'black',
                                borderBottomWidth: 1,
                            }}
                        />

                        {props.mode === 'edit' ?
                        (<View style={{ flexDirection: 'column', justifyContent: 'space-evenly', paddingTop: 50 }}>
                             
                            <DigitalPassTextInput label={'Reason'} onInputChange={onReasonEntered} />
                            <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', paddingTop: 50 }}>
                                <Button title="Approve" onPress={() => props.handleApprove(adminReason)} />
                                <Button title="Reject" onPress={() => props.handleReject(adminReason)} />
                            </View>
                            </View>
                            ) :
                            (
                                <Button title="Close" onPress={() => props.handleReject('')} />
                            )

                        }
                    </View>

                </Modal>
            </View>
            :

            <React.Fragment />
    );
};

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    }
});