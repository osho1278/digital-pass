import {
    StyleSheet
  } from 'react-native';
export const styles = StyleSheet.create({
    container: {
       padding:20,  
       width:'100%',
      // flex: 1,
      justifyContent: 'center',
      // alignItems: 'center',
      // // backgroundColor: '#DCDCDC',
    },
    inputContainer: {
  
        borderBottomColor: '#F5FCFF',
        color: '#F5FCFF',
        backgroundColor:  "#193F78",
        borderRadius:10,
        borderBottomWidth: 1,
        //width:250,
        //height:45,
        marginBottom:20,
        flexDirection: 'row',
        alignItems:'center'
    },
    inputs:{
        height:45,
        marginLeft:16,
        borderBottomColor: '#FFFFFF',
        flex:1,
        color:'orange',
        fontSize:22
    },
    inputIcon:{
      width:30,
      height:30,
      marginLeft:15,
      justifyContent: 'center'
    },
    buttonContainer: {
      height:45,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom:20,
      width:'100%',
      borderRadius:30,
    },
    loginButton: {
      backgroundColor: "#00b5ec",
    },
    loginText: {
      color: 'white',
    }
  });
   