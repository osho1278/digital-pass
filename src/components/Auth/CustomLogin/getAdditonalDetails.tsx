import React, { Component, useState } from "react";
import {
    Alert,
    Modal,
    StyleSheet,
    Text,
    TouchableHighlight,
    View,
    Button,
    Image,
    Dimensions,
    BackHandler
} from "react-native";
import { DigitalPassTextInput } from "../../Input";
import { DigitalPassCamera } from "../../Camera";
import {uploadImageToFirebaseStorage} from '../../../utils/imageUpload';
import {updateUser} from '../../../utils/user';
import Spinner from 'react-native-loading-spinner-overlay';

interface Props {
    onAdditionalDetailsEntered: Function;
    user: any;
}
interface State {
    showDialog?: boolean;
    user: any;
    displayName?: string;
    phoneNumber?: string;
    email?: string;
    photoURL: string;
    uid: string;
    openCamera: boolean;
    isSubmitting?:boolean;
}
export class GetAdditionalDetails extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        const { _user } = this.props.user;
        console.log("Get additional details",_user);
        // const { _auth } =_user;
        this.state = { ..._user, openCamera: false, isSubmitting:false };
    }

    handleSubmit=async ()=>{
        this.setState({isSubmitting:true})
        let uploadedPhotoURL = await uploadImageToFirebaseStorage(this.state.uid , this.state.photoURL, 'profilePic');
        let payload={
            displayName:this.state.displayName,
            email:this.state.email,
            phoneNumber:this.state.phoneNumber,
            photoURL:uploadedPhotoURL
        }
        
        await updateUser(this.state.uid,payload);
        this.props.onAdditionalDetailsEntered(this.state.uid);

        
    }    
    
    componentWillUnmount(){
        this.setState({isSubmitting:false})
    }



    render() {
        const { displayName, phoneNumber, email, photoURL, uid } = this.state;
        return (

            this.props.user ?
                <View>
                      <Spinner
          visible={this.state.isSubmitting}
          textContent={'Loading...'}
          textStyle={{color: '#FFF'}}
        />
                    <Modal
                        animationType="slide"
                        transparent={false}
                        visible={true}
                        onRequestClose={() => {
                            // Alert.alert("Modal has been closed.");
                        }}
                    >
                        <View style={{ padding: 20 }} >
                            <Text style={{ fontSize: 30 }}>{'Details Verification'}</Text>
                        </View>

                        <View style={{ padding: 20 }} >
                            <Text style={{ fontSize: 22 }}>{`A  verification email has been sent to you on your email address ${email}`}</Text>
                        </View>

                        <View style={{ padding: 20 }} >
                            <View style={{ padding: 5 }} >
                                <Text>{`Your UID is ${uid}`}</Text>
                            </View>
                            <View style={{ padding: 5 }} >
                                <DigitalPassTextInput
                                    label={`${displayName}`}
                                    numberOfLines={1}
                                    placeholder={`${'Enter Display Name'}`}
                                    onInputChange={(text: string) => this.setState({ displayName: text })}
                                />
                            </View>
                            <View style={{ padding: 5 }} >
                                <DigitalPassTextInput
                                    label={''}
                                    numberOfLines={1}
                                    placeholder={`${'Enter Phone number'} `}
                                    onInputChange={(text: string) => this.setState({ phoneNumber: text })}
                                />
                            </View>
                           
                            <View style={{ padding: 5 }} >
                                {photoURL ?
                                    <Image
                                        style={{ borderWidth: 2, height: 300 }}
                                        source={{
                                            uri: photoURL
                                        }} />
                                    :
                                    <Button
                                        title="Upload Picture"
                                        onPress={() => this.setState({ openCamera: true })}
                                    />
                                }
                            </View>
                            <View style={{ padding: 5 }} >
                                <Button
                                    title="Submit"
                                    color='green'
                                    onPress={() => this.handleSubmit()}
                                />
                            </View>

                            <Modal
                                animationType="slide"
                                transparent={true}
                                visible={this.state.openCamera}
                                onRequestClose={() => {
                                    // Alert.alert("Modal has been closed.");
                                }}
                            >

                                <DigitalPassCamera
                                    // @ts-ignore
                                    cameraType={this.state.uploadID ? 'back' : 'front'}
                                    getimageUri={(photoURL: string) => {
                                        this.setState({ photoURL })
                                        this.setState({ openCamera: false });
                                    }} />

                            </Modal>
                        </View>
                    </Modal>

                </View>
                :

                <React.Fragment />
        );
    }
};

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    }
});