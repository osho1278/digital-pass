import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableHighlight,
  Image,
  Alert
} from 'react-native';
import { styles } from './styles';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import auth from '@react-native-firebase/auth';
import { GetAdditionalDetails } from './getAdditonalDetails';
import { userSignInSuccess, userSignInSuccessPayload } from '../../../actions/auth';
import { createUser, getUser } from '../../../utils/user';
import Spinner from 'react-native-loading-spinner-overlay';
import { Toast } from 'native-base';

export interface Props {
  userSignInSuccess: Function;
}
export interface State {
  email: string;
  password: string;
  isAdditionalDetailsNeeded?: boolean;
  user?: any;
  formSubmitting:boolean;
}
class CustomLoginView extends Component<Props, State> {

  constructor(props: Props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      formSubmitting:false
    }
  }

  onClickListener = (viewId: any) => {
    auth()
      .createUserWithEmailAndPassword(this.state.email, this.state.password)
      .then((newUser) => {
        let {user} = newUser;
        console.log('User account created & signed in!', user);
        user.sendEmailVerification();
        // @ts-ignore
        createUser(user._user);
        this.setState({ isAdditionalDetailsNeeded: true, user });
        Toast.show({
          text: "Created new user, additional details needed .",
          buttonText: "Okay",
          duration: 10000
        })
      })
      .catch(error => {
        if (error.code === 'auth/email-already-in-use') {
          auth()
            .signInWithEmailAndPassword(this.state.email, this.state.password)
            .then((returningUser:any) => {
              console.log('User already existed and his details are ', returningUser);
              // @ts-ignore
              const {user}=returningUser;
              // this.setState({ isAdditionalDetailsNeeded: true, user});
              
              this.setState({ user });
              this.updateApplicationState(user);
              Toast.show({
                text: "Signed in successfully.",
                buttonText: "Okay",
                duration: 10000
              })
            })
          console.log('That email address is already in use!');
        }

        if (error.code === 'auth/invalid-email') {
          console.log('That email address is invalid!');
          Toast.show({
            text: "Invalid email or password !",
            buttonText: "Okay",
            duration: 10000
          })
        }

        console.log(error);
      });
  }

  updateApplicationState = (user: any) => {
    let payLoad: userSignInSuccessPayload = {
      OnSuccess: () => { this.setState({isAdditionalDetailsNeeded:false})},
      OnFailure: () => { },
      data: user
    }
    this.props.userSignInSuccess(payLoad);
  }

  onAdditionalDetailsEntered=async (uid:string)=>{
    let user= await getUser(uid);
    // @ts-ignore
    const {_data}  = user;
    this.updateApplicationState({_user:_data});

  }

  render() {
    return (
      <View style={styles.container}>
        <Spinner
          visible={this.state.formSubmitting}
          textContent={'Loading...'}
          textStyle={{color: '#FFF'}}
        />
        <View style={styles.inputContainer}>
          <Icon name={'ios-mail'} size={30} style={{ padding: 10, color: 'white' }} />
          <TextInput style={styles.inputs}
            placeholder="Email"
            keyboardType="email-address"
            underlineColorAndroid='transparent'
            onChangeText={(email) => this.setState({ email })} />
        </View>

        <View style={styles.inputContainer}>
          <Icon name={'ios-key'} size={30} style={{ padding: 10, color: 'white' }} />
          <TextInput style={styles.inputs}
            placeholder="Password"
            secureTextEntry={true}
            underlineColorAndroid='transparent'
            onChangeText={(password) => this.setState({ password })} />
        </View>

        <TouchableHighlight style={[styles.buttonContainer, styles.loginButton]} 
        disabled={this.state.formSubmitting}
        onPress={() => {
          if(this.state.email && this.state.password){
          this.setState({formSubmitting:true})
          this.onClickListener('login')
          }else{
            Toast.show({
              text: "Missing email or password data !",
              buttonText: "Okay",
              duration: 10000
            })
          }
        }}>
          <Text style={styles.loginText}>{`Login/SignUp`}</Text>
        </TouchableHighlight>

        {this.state.isAdditionalDetailsNeeded && 
        <GetAdditionalDetails 
        user={this.state.user} 
        onAdditionalDetailsEntered={this.onAdditionalDetailsEntered} />}
      </View>
    );
  }
}

const mapStateToProps = (state: any) => {
  const { auth } = state;
  return {
    auth: auth,
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    userSignInSuccess: (payload: userSignInSuccessPayload) => dispatch(userSignInSuccess(payload))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps)(CustomLoginView)

