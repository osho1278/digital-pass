import React from 'react';
import { View } from 'react-native';
import auth from '@react-native-firebase/auth';
import { connect } from 'react-redux'
import { LoginManager, AccessToken } from 'react-native-fbsdk';
import { userSignInSuccess, userSignInSuccessPayload } from '../../actions/auth';
import firestore from '@react-native-firebase/firestore';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Text } from 'native-base';
import { SocialIcon } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';

interface Props {
  userSignInSuccess?: any;
  navigation?: any;
  onSignInSuccess?: Function;
}
interface State{
  formSubmitting:boolean;
}

class FacebookSignIn extends React.Component<Props,State> {

  constructor(props: any) {
    super(props);
    this.state={formSubmitting:false};
  }

  authStateChangeHandler = async (user: any) => {
    let payloadUser: any;
    if (!user) { return }
    try {
      const { _user } = user;
      if (_user && _user.uid) {
        const path = '/users/' + _user.uid;
        // const reference = database().ref(path).update({ profile: JSON.stringify(_user) });
        let doesUserExist = await firestore()
          .collection('Users')
          .doc(_user.uid)
          .get();
        // @ts-ignore 
        const { _data } = doesUserExist;
        if (!_data) {
          let curUser = { ..._user, role: 'USER' };
          payloadUser = curUser;

          await firestore()
            .collection('Users')
            .doc(_user.uid)
            .set({
              ...payloadUser
            })
            .then(() => {
              // this.updateState(_data);
              this.updateState({ _user: payloadUser });
            })


        } else {
          // @ts-ignore
          payloadUser = doesUserExist._data;
          this.updateState({ _user: payloadUser });

        }



        // send data to reducer

      }
    } catch (e) {
      console.log('authStateChangeHandler', e);
    }



  }
  updateState = (user: any) => {

    let payLoad: userSignInSuccessPayload = {
      OnSuccess: () => { },
      OnFailure: () => { },
      data: user
    }
    this.props.userSignInSuccess(payLoad);
  }
  onFacebookButtonPress = async () => {
    // Attempt login with permissions
    const result = await LoginManager.logInWithPermissions(['public_profile', 'email']);

    if (result.isCancelled) {
      throw 'User cancelled the login process';
    }

    // Once signed in, get the users AccesToken
    const data = await AccessToken.getCurrentAccessToken();

    if (!data) {
      throw 'Something went wrong obtaining access token';
    }

    const facebookCredential = auth.FacebookAuthProvider.credential(data.accessToken);

    // Create a Firebase credential with the AccessToken


    // Sign-in the user with the credential
    auth().onAuthStateChanged(this.authStateChangeHandler)
    return auth().signInWithCredential(facebookCredential);
  }

  render() {
    return (
      // <TouchableOpacity

      //   style={{width:200, 
      //     borderRadius:5, 
      //     backgroundColor:'#F5FCFF',
      //     justifyContent:'center',
      //     alignItems:'center',

      //   }}
      //   onPress={() => this.onFacebookButtonPress()
      //     .then(() => console.log('Signed in with Facebook!'))
      //   }
      // >
      <View style={{ flex: 1, width: '100%', justifyContent: 'flex-end' }}>
         <Spinner
          visible={this.state.formSubmitting}
          textContent={'Loading...'}
          textStyle={{color: '#FFF'}}
        />
        <SocialIcon
          title='Sign In With Facebook'
          button
          type='facebook'
          onPress={() => this.onFacebookButtonPress()
            .then(() => console.log('Signed in with Facebook!'))
          }
        />
      </View>
      // </TouchableOpacity>
    );
  }
}
const mapStateToProps = (state: any) => {
  const { auth } = state;
  return {
    auth: auth,
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    userSignInSuccess: (payload: userSignInSuccessPayload) => dispatch(userSignInSuccess(payload))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps)(FacebookSignIn)
