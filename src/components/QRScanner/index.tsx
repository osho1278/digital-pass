import React, { Component } from 'react';

import {
  StyleSheet,
  Text,
  TouchableOpacity,
  Linking,
  Alert
} from 'react-native';

import QRCodeScanner from 'react-native-qrcode-scanner';
import { DigitalPassModal } from '../Dialog/Modal';
interface Props { }
interface State {
  showModal: boolean;
  item?:any;
}
export class ScanScreen extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { showModal: false }
  }

  onSuccess = (e: any) => {
   
    this.setState({showModal:true, item:JSON.parse(e.data)});
   
  };

  render() {
    return (
      this.state.showModal ?

        <DigitalPassModal
          mode={'view'}
          title={'Approve/Reject'}
          item={this.state.item}
          description={'Confirmation needed'}
          showDialog={this.state.showModal}
          handleApprove={()=>{this.setState({showModal:false})}}
          handleReject={()=>{this.setState({showModal:false})}}
        />
        :

        <QRCodeScanner
          onRead={this.onSuccess}
          // flashMode={QRCodeScanner.Constants.FlashMode.torch}
          // topContent={
          //   <Text style={styles.centerText}>
          //     Go to{' '}
          //     <Text style={styles.textBold}>wikipedia.org/wiki/QR_code</Text> on
          //   your computer and scan the QR code.
          // </Text>
          // }
          // bottomContent={
          //   <TouchableOpacity style={styles.buttonTouchable}>
          //     <Text style={styles.buttonText}>OK. Got it!</Text>
          //   </TouchableOpacity>
          // }
        />
    );
  }
}

const styles = StyleSheet.create({
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777'
  },
  textBold: {
    fontWeight: '500',
    color: '#000'
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)'
  },
  buttonTouchable: {
    padding: 16
  }
});
