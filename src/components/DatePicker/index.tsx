import React, { Component } from 'react';
import {  DatePicker, Text } from 'native-base';
import {View} from 'react-native';
import moment from 'moment';
import Icon from 'react-native-vector-icons/Ionicons';
import { TouchableOpacity } from 'react-native-gesture-handler';
interface Props{
default:any;
label:string;  
onDateSelected:Function;
}
interface State{
    chosenDate:any
}
export class DigitalPassDatePicker extends React.Component<Props>{
 props:Props;
 constructor(props:Props){
   super(props);
   this.props=props;
 }
  // setDate(newDate:any) {
  //   this.setState({ chosenDate: newDate });
  // }

    
render(){
    return (
      <TouchableOpacity
      // @ts-ignore
      onPress={()=>this.refs.datePicker.showDatePicker()}
      >
        <View style={{flexDirection:'row', justifyContent:'center', alignItems:'center'}}>

       <Icon name='ios-calendar' 
       size={30}
        />
    <Text style={{fontSize:20}}>{`   ${this.props.label} `}</Text>
   
          <DatePicker
            defaultDate={new Date(this.props.default)}
            ref={'datePicker'}
            locale={"en"}
            timeZoneOffsetInMinutes={undefined}
            modalTransparent={false}
            animationType={"fade"}
            androidMode={"default"}
            // placeHolderText={props.label}
            textStyle={{ color: "transparent", height:0 }}
            placeHolderTextStyle={{ color: "transparent" , fontWeight:'bold', height:0 }}
            onDateChange={(newDate)=>{
              this.props.onDateSelected(newDate);
            }}
            disabled={false}
            />
            </View>
            </TouchableOpacity>
    );
}
}