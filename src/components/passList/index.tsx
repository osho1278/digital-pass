import React from 'react';
import { View, RefreshControl } from 'react-native';
import { List, ListItem, Left, Body, Right, Toast, Text } from 'native-base';
import firestore from '@react-native-firebase/firestore';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import { Pass } from '../../models/pass';
import Icon from 'react-native-vector-icons/Ionicons';
import moment from 'moment';
interface Props {
    params?: any;
    profile?: any;
    navigation?: any;
    route?: any;
    onPassClickHandler?: any
    shouldUpdate?:boolean;
}

interface State {
    profile?: any;
    reason?: string;
    passes?: any[];
    validity?: string; // '2020-04-20T00:00:00Z',
    refreshing: boolean
}
export class PassesList extends React.Component<Props, State>{
    onPassClickHandler:any;
    constructor(props: Props) {
        super(props);
        // this.state = { passes: [], refreshing: false };
        if (props.profile) {
            // this.setState({ profile: props.profile });
            this.state = { passes: [], refreshing: false,profile: props.profile };
            const { onPassClickHandler } = this.props;
            this.onPassClickHandler = onPassClickHandler;
        } else {
            // check if its in route
            try{
            const { route } = props;

            const { params } = route;
            if (params.profile) {
                // this.setState({ profile:  });
                this.state = { passes: [], refreshing: false,profile: params.profile };

            }
            this.onPassClickHandler = params.onPassClickHandler;
        }catch(e){
            console.log(e);
        }

        }

        this.getPassInformation();
    }
    componentWillReceiveProps(props:Props){
        this.onRefresh();
    }

    onRefresh = () => {
        // this.setState({ refreshing : !this.state.refreshing});
        this.getPassInformation();
    }

    getPassInformation = async () => {
        this.setState({ refreshing: true });
        try {
            const { uid } = this.state.profile;
            const user = await firestore()
                .collection('Users')
                .doc(uid)
                .get();
            // @ts-ignore
            let { passes } = user._data;
            this.setState({ passes: passes });
            this.setState({ refreshing: false });

        } catch (e) {
            // this.props.navigation.navigate('Login');
        }
    }

    componentDidMount() {

        this.getPassInformation();
    }

    render() {
       const {profile} = this.state;
        return (
            <ScrollView refreshControl={
                <RefreshControl refreshing={this.state.refreshing} onRefresh={this.onRefresh} />
            }>
            <View style={{ flex: 1, padding: 10 }} >
                {this.state.passes && this.state.passes.length > 0 ?
                    
                        <List key={'new list'}>
                            {this.state.passes.map((item: Pass,index) => {
                                return (
                                    <TouchableOpacity 
                                    key={`btn-key-${index}`}
                                    onPress={() => {
                                            this.onPassClickHandler({...item,index, uid:profile.uid})        
                                    }}>
                                        <ListItem avatar key={`pass-list-${index}`}>
                                            <Left>
                                                <Icon
                                                    name={item.isApproved ? 'ios-checkmark' : 'ios-close'}
                                                    color={item.isApproved ? 'green' : 'red'}
                                                    size={50} />
                                            </Left>
                                            <Body>
                                                <Text>{item.reason || ''}</Text>
                                                <Text note>{item.adminReason || 'Admin has not approved yet'}</Text>
                                                <Text note>{`${moment.unix(item.toDate).format("dddd, MMMM Do YYYY, h:mm:ss a")}`}</Text>
                                            </Body>
                                            <Right>
                                            </Right>
                                        </ListItem>
                                    </TouchableOpacity>
                                )
                            })}
                        </List>
                   
                    :
                    <View style={{ flex: 1, justifyContent: 'center', padding: 20, }}>
                        <Text style={{ fontSize: 22, fontWeight: 'bold' }}>
                            {`You currently don't have any pass active , you can request for one now ! `}
                        </Text>
                    </View>
                }

            </View>
            </ScrollView>
        );
    };
}
