import React from 'react';
import { TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

export const DrawerButton = (props: any) => {
  let { navigation } = props;
  return (

    <TouchableOpacity
      onPress={() => navigation.openDrawer()}
    >
      <Icon name="ios-menu" size={30} color="#4F8EF7" style={{ paddingLeft: 10 }} />

    </TouchableOpacity>
  )
};
export default DrawerButton;