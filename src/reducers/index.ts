import { combineReducers } from 'redux';
import auth from './auth';
import location from './location'; 

const login = combineReducers({
    auth,
    location
  
  })
export default login;