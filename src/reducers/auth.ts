import {
  AUTH_SUCCESS,
  USER_LOGOUT
} from '../constants/auth'
import App from '../../App';

//const { SHOW_ALL } = VisibilityFilters

export default function auth(state = [], action: any){
  switch (action.type) {
    case AUTH_SUCCESS:
      console.log('At reducer adding data AUTH_SUCCESS ',action.payload.data)
      return action.payload.data
    case USER_LOGOUT:
      console.log('Logging out',action.payload)
      action.payload.OnSuccess();
      return [];
    default:

      return state
  }
}