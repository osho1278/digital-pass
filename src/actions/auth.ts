import {
    AUTH_SUCCESS,
    USER_LOGOUT
} from '../constants/auth'
import Payload from '../models/payload';
import { action } from 'typesafe-actions';

export interface userSignInSuccessPayload extends Payload<any> {}
export interface userLogoutSuccessPayload extends Payload<any> {}


export const userSignInSuccess = (payload: userSignInSuccessPayload) => action(AUTH_SUCCESS, payload);
export const userLogoutSuccess = (payload:userLogoutSuccessPayload ) => action(USER_LOGOUT, payload);
