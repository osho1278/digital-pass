import {
   UPDATE_LOCATION
} from '../constants/location'
import Payload from '../models/payload';
import { action } from 'typesafe-actions';

export interface userUpdateLocationPayload extends Payload<any> {}


export const userUpdateLocationSuccess = (payload: userUpdateLocationPayload) => action(UPDATE_LOCATION, payload);
