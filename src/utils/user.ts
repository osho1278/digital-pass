import firestore from '@react-native-firebase/firestore';

export const createUser=async (payload:any)=>{
    console.log('creating user with uid',payload.uid,' and payload',payload);
    await firestore()
    .collection('Users')
    .doc(payload.uid)
    .set({
      ...payload
    })
    .then(() => {
    })

}

export const getUser=async (uid:any)=>{
  console.log('getting user with uid',uid);
  let user = await firestore()
  .collection('Users')
  .doc(uid)
  .get();
  console.log('Got user',user);
  return user;
}

export const updateUser=async (uid:string,payload:any)=>{
    console.log('updating user with uid',uid,' and payload',payload);
    await firestore()
    .collection('Users')
    .doc(uid)
    .update({
      ...payload
    })
    .then(() => {
    })

}