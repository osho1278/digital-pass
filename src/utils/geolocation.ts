import database from '@react-native-firebase/database';

export const updateGeoLocation=async (uid:string,payload:any)=>{
    console.log('updating user with uid',uid,' and payload',payload);
    const reference = database().ref('/geolocation/'+uid).push();

  reference
  .set({
    location:payload,
  })
  .then(() => console.log('Data updated.'));

}

export function distance(lat1:any, lon1:any, lat2:any, lon2:any, unit='K')
{var R = 6371; // Radius of the earth in kilometers
var dLat = deg2rad(lat2 - lat1); // deg2rad below
var dLon = deg2rad(lon2 - lon1);
var a =
  Math.sin(dLat / 2) * Math.sin(dLat / 2) +
  Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
  Math.sin(dLon / 2) * Math.sin(dLon / 2);
var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
var d = R * c; // Distance in KM
return d;
}

function deg2rad(deg:any) {
return deg * (Math.PI / 180)
}


