import storage from '@react-native-firebase/storage';

export  const makeid = (length: number) => {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

export const uploadImageToFirebaseStorage = async (uid: string, imagePath: string, typeOfImage: string) => new Promise((resolve, reject) => {
    let randId = makeid(10)
    const reference = storage().ref('/images/' + uid + '/' + typeOfImage + '_' + randId);
    try {
      const task = reference.putFile(imagePath);

      task.on('state_changed', taskSnapshot => {
        // @ts-ignore
        console.log(`${taskSnapshot.bytesTransferred} transferred out of ${task.totalBytes}`);
      });

      task.then(async () => {
        // @ts-ignore
        let imgPath = await reference.getDownloadURL()
        resolve(imgPath);

      });
    } catch (e) {
      reject(e);
    }

  });