export interface Profile {
    displayName:string;
    email: string;
    emailVerified: boolean, 
    isAnonymous: boolean, 
    metadata: Object, 
    phoneNumber: string, 
    photoURL: string;
    providerData: []; 
    providerId:string;
    role: string;
    uid: string;
  }
  