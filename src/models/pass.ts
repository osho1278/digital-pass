export interface Pass{
    userId?:string
    reason:string;
    adminReason?:string;
    fromDate:any;
    toDate?:any;
    isApproved:boolean;
    image_id?:string;
    image_selfie?:string;
}
