interface Payload<DataType> {
    data?: DataType;
    OnSuccess?: Function;
    OnFailure?: Function;
}

export default Payload;