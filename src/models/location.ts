
interface Coords{
    latitude?:any;
    longitude?:any;
    accuracy?:String;
}
export interface Location{
    timeStamp:string
    coords:Coords;
}